﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldController : MonoBehaviour {

    public static WorldController instance;
    public Sprite[] backgrounds;
    private SpriteRenderer spRend;
    private int curBack = 0;

    public void ChangeBackGroundRandom()
    {
        int newBack;

        do
            newBack = Random.Range(0, backgrounds.Length);
        while (newBack == curBack);

        curBack = newBack;
        spRend.sprite = backgrounds[newBack];
    }

    void Start()
    {
        instance = this;
        spRend = GetComponent<SpriteRenderer>();
    }
}
