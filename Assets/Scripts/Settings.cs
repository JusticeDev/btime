﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Settings : MonoBehaviour {
    public static bool swapped = false;
    public static bool countDisabled = false;
    public static float musicVolume = 0.1f;
    public static float sfxVolume = 1f;

    public Slider musicVolumeSlider;
    public Toggle swapToggle;
    public Toggle countDownToggle;
    public Slider sfxVolumeSlider;
    AudioSource audioPlayer;
    public AudioClip sfxSample;
    public AudioClip musicSample;

    void Start()
    {
        swapToggle.isOn = swapped;
        countDownToggle.isOn = countDisabled;

        musicVolumeSlider.value = musicVolume;
        sfxVolumeSlider.value = sfxVolume;

        audioPlayer = gameObject.GetComponent<AudioSource>();
        audioPlayer.ignoreListenerVolume = true;
    }

    public void SetSwapped()
    {
        //Эт переключатель положения кнопок в игре
        swapped = swapToggle.isOn;
    }

    public void SetCountdownActive()
    {
        //Включить/отключить отсчёт перед началом игры
        countDisabled = countDownToggle.isOn;
    }

    public void PlayMusicSample()
    {
        audioPlayer.Stop();
        audioPlayer.volume = musicVolume;
        audioPlayer.clip = musicSample;
        audioPlayer.Play();
    }

    public void PlaySFXSample()
    {
        audioPlayer.Stop();
        audioPlayer.volume = sfxVolume;
        audioPlayer.clip = sfxSample;
        audioPlayer.Play();
    }

    public void SetMusicVolume()
    {
        //Устанавливаем громкость
        musicVolume = musicVolumeSlider.value;
        //Проигрываем сэмпл мелодии
        PlayMusicSample();
    }

    public void SetSFXVolume()
    {
        //Устанавливаем громкость
        sfxVolume = sfxVolumeSlider.value;
        //Проигрываем сэмпл мелодии
        PlaySFXSample();
    }
}
