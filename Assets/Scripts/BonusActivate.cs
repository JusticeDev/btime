﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusActivate : MonoBehaviour {

    //Чё за бонус сейчас
    public static bool hasBonus;

    public GameObject projDestroyer;

    private AudioSource audioSource;

    private int destCount = 8;

    //Замедлено ли время
    //private bool slowtime = false;

    //Transform spawnPos;

    // Use this for initialization
    void Start () {
        //spawnPos = GameObject.FindGameObjectWithTag("Respawn").transform;
        hasBonus = false;
        audioSource = GetComponent<AudioSource>();
	}

    //Функция взрыва ебаной бомбы
    void Explode()
    {
        for (int i = 0; i < destCount; i++)
        {
            var clone = Instantiate(projDestroyer, transform.position, Quaternion.Euler(0, 0, (-360 / destCount) * i));
            clone.GetComponent<ProjectileDestroyerManager>().direction = i;
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.E))
            UseBonus();
    }

    public void UseBonus()
    {
        if (!Game.paused)
        {
            if (!Player.isDead)
            {
                if (hasBonus)
                {
                    Explode();
                    audioSource.Play();
                    hasBonus = false;
                }
            }
        }
    }
}
