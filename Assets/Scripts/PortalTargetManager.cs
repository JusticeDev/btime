﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalTargetManager : MonoBehaviour {

    public static float startTime; //Время рождения портала-"цели"
    public static float lifeTime = 6; //Жизнь портала-"цели"
    public static int curSelected; //Номер текущего портала-"цели"
    static int newChoice; //Для рандома новой цели
    public static int targetsHit; //Сколько было поражено порталов
    public static int targetsNeeded; //Сколько нужно поразить порталов, чтобы тебе дали бомбу
    private AudioSource audioSource;

    void OnEnable()
    {
        if (Player.bombLevel == 1)
            targetsNeeded = 4;
        else
            targetsNeeded = 5;
    }

    // Use this for initialization
    void Start () {
        targetsHit = 0;
        curSelected = 0;
        newChoice = 0;

        SetTarget(Game.startline);
        audioSource = GetComponent<AudioSource>();
    }


    public static void SetTargetRandom()
    {
        //Выбираем новую линию, но не ту, что была до этого
        do
        {
            if (!Game.flipped)
                newChoice = new System.Random().Next(0, Game.lineCount);
            else
                newChoice = new System.Random().Next(Game.lineCount + 1, Game.lineCount * 2);
        }
        while (newChoice == curSelected);

        curSelected = newChoice;
        startTime = Game.curTime;
    }

    public static void SetTarget(int target)
    {
        curSelected = target;
        startTime = Game.curTime;
    }

	// Update is called once per frame
	void Update () {
        if (!Game.paused)
        {
            if (!Player.isDead)
            {
                //Если игрок набил достаточное кол-во, выдаём ему бонус
                if (targetsHit >= targetsNeeded)
                {
                    targetsHit = 0;
                    BonusActivate.hasBonus = true;
                    audioSource.Play();
                }

                //Отнимаем хп и наказываем игрока, если он не успел вовремя попасть в портал
                if (Game.curTime - startTime >= lifeTime)
                {
                    SetTargetRandom();
                    Player.curlives--;
                    EventManager.HealthLost();
                }
            }
        }
	}
}
