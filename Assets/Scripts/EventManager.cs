﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EventManager : MonoBehaviour {

    public delegate void OnHealthLost();
    public static event OnHealthLost OnHPLost;

    public static void HealthLost()
    {
        if (OnHPLost != null)
            OnHPLost();
    }
}
