﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    private static Animator anim;   //Референс компонента Аниматора 
 
    private static Transform spawnPos; //Компонент позиции, где мы спавнимся
    private static SpriteRenderer spriteRend; //Это наш компонент-рендерер спрайтов

    public static List<Vector2> allowedPositions; //Координаты линий

    //Флаги
    public static bool isDead;    //ты живой там, не?

    //Линия
    private static int curline;   //На какой линии стоим

    //Здоровье
    public static int curlives; //здоровье, логично, да
    public static int maxlives = 3; //Максимальное здоровье, ого
    private static int startlives = 3; //Начальное здоровье, ого
   
    //Неуязвимость героя
    public static bool immortal;
    public static float immTimeStart;
    public static float immTime = 2;

    public float betweenLinesDist = 1.3f; //Расстояние между линиями

    //Уровень вкачки скиллов игрока
    public static int immortalLevel = 0;
    public static int scoreX2Level = 0;
    public static int timeSlowLevel = 0;
    public static int bombLevel = 0;
    public static int coins;

    public AudioClip loseSound;
    public AudioClip moveSound;
    public AudioClip hitSound;

    private AudioSource audioSource;

    void OnEnable()
    {
        EventManager.OnHPLost += LostHP;
    }

    void OnDisable()
    {
        EventManager.OnHPLost -= LostHP;
    }

    IEnumerator TurnRed()
    {
        spriteRend.color = Color.red;
        yield return new WaitForSeconds(immTime);
        spriteRend.color = Color.white;
        yield return null;
    }

    void LostHP()
    {
        if (curlives > 0)
        {
            BonusAuto.SetImmortalityTimer(immTime);
            StartCoroutine(TurnRed());
            audioSource.clip = hitSound;
            audioSource.Play();
        }
    }

    // Use this for initialization
    void Start()
    {
        isDead = false;
        immortal = false;

        //Получаем референс аниматора
        anim = GetComponent<Animator>();

        //Референс спавна
        spawnPos = GameObject.FindGameObjectWithTag("Respawn").transform;
        spriteRend = GetComponent<SpriteRenderer>();

        //Относительно надёжный способ хранения координат, на которые мы будем прыгать
        allowedPositions = new List<Vector2>();
        for (int i = 0; i < Game.lineCount; i++)
            allowedPositions.Add(new Vector2(spawnPos.position.x + betweenLinesDist * i, spawnPos.position.y));

        //Ставим нашего персонажа на нужную линию
        transform.position = allowedPositions[Game.startline];

        //Выставим начальные значения для переменных текущего состояния
        curline = Game.startline;
        curlives = startlives;

        audioSource = GetComponent<AudioSource>();
    }

    public static void SetImmortality(bool val)
    {
        immortal = val;
        if (!val)
            spriteRend.enabled = true;
    }

    IEnumerator Die()
    {
        isDead = true;
        Game.gameInstance.Pause();
        anim.SetTrigger("Die");
        audioSource.clip = loseSound;
        audioSource.Play();
        //Отключим бонус замедления времени
        Time.timeScale = 1;
        yield return new WaitForSeconds(1);
        Game.gameInstance.ShowLostMenu();

        yield return null;
    }

    // Update is called once per frame
    void Update()
    {
        if (!Game.paused)
        {
            if (!isDead)
            {
                //Если здоровье дропнуло до 0, то герой умирает
                if (curlives <= 0)
                    StartCoroutine(Die());

                if (Input.GetKeyDown(KeyCode.RightArrow))
                    MoveRight();
                else
                if (Input.GetKeyDown(KeyCode.LeftArrow))
                    MoveLeft();

                if (immortal)
                    SpriteBlinkingEffect();
            }
        }
    }

    public void MoveLeft()
    {
        if (!isDead && !Game.paused)
        {
            if (curline > 0)
            {
                curline--;
                transform.position = allowedPositions[curline];
                anim.SetTrigger("Flap");
                audioSource.clip = moveSound;
                audioSource.Play();
            }
        }
    }

    public void MoveRight()
    {
        if (!isDead && !Game.paused)
        {
            if (curline < allowedPositions.Count - 1)
            {
                curline++;
                //Двигаемся по линиям
                transform.position = allowedPositions[curline];
                anim.SetTrigger("Flap");
                audioSource.clip = moveSound;
                audioSource.Play();
            }
        }
    }

    public float spriteBlinkingTimer = 0.0f;
    public float spriteBlinkingMiniDuration = 0.1f;
    public float spriteBlinkingTotalTimer = 0.0f;
    public float spriteBlinkingTotalDuration = 2.0f;

    private void SpriteBlinkingEffect()
    {
        spriteBlinkingTimer += Time.deltaTime;
        if (spriteBlinkingTimer >= spriteBlinkingMiniDuration)
        {
            spriteBlinkingTimer = 0.0f;
            if (gameObject.GetComponent<SpriteRenderer>().enabled == true)
                gameObject.GetComponent<SpriteRenderer>().enabled = false; 
            else
                gameObject.GetComponent<SpriteRenderer>().enabled = true; 
        }
    }
}
