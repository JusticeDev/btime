﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BonusGUI : MonoBehaviour {

    //Здесь Гуи Элемент Бонусов

    // Update is called once per frame
    void Update () {
        //Меняем картинку ГУИ соответственно бонусу
        //GetComponent<Image>().sprite = bonusSprite[BonusActivate.bonus];

        if (BonusActivate.hasBonus)
            GetComponent<Renderer>().material.color = Color.white;
        else
            GetComponent<Renderer>().material.color = Color.grey;
    }
}
