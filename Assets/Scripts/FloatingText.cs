﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class FloatingText : MonoBehaviour {

    public float moveToY;
    public float moveSpeed;
    float movedY;
    public int scoreAdded;
    Transform text;
	
	// Update is called once per frame
	void Update () {
        if (!Game.paused)
        {
            GetComponentInChildren<Text>().text = "+" + scoreAdded;

            if (movedY < moveToY)
            {
                movedY += moveSpeed * Time.deltaTime;
                transform.position = new Vector2(transform.position.x, transform.position.y + movedY);
            }
            else
                Destroy(gameObject);
        }
	}
}
