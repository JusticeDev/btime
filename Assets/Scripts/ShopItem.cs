﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopItem : MonoBehaviour {

    public Text costText;
    public Text curText;
    public int itemID;
    public int maxLevel;
    public int startCost;
    public Image coinImage;
    int curLevel;
    float curValue;
    private int curCost;

	// Use this for initialization
	void Start () {
        RefreshItem();
	}
	
    void RefreshItem()
    {
        switch(itemID)
        {
            case 1:
                curLevel = Player.immortalLevel;
                curValue = BonusAuto.defaultImmDur;
                break;
            case 2:
                curLevel = Player.timeSlowLevel;
                curValue = BonusAuto.defaultSlowDur;
                break;
            case 3:
                curLevel = Player.scoreX2Level;
                curValue = ScoreManager.defaultLifeTime;
                break;
            case 4:
                curLevel = Player.bombLevel;
                break;
        }

        if (curLevel != maxLevel)
        {
            //Повышаем цену в геометрической прогрессии от уровня
            curCost = startCost;

            for (int i = 0; i < curLevel; i++)
                curCost *= 2;

            costText.text = "" + curCost;
        }
        else
        {
            costText.text = "Sold Out";
            coinImage.enabled = false;
        }

        if (curText != null)
        {
            float dur = curValue + curLevel;
            curText.text = "Cur Duration: " + dur + " sec";
        }
    }

    public void BuyItem()
    {
        if (curLevel < maxLevel)
        {
            if (Player.coins >= curCost)
            {
                Player.coins -= curCost;

                switch (itemID)
                {
                    case 1:
                        Player.immortalLevel++;
                        curLevel++;
                        break;
                    case 2:
                        Player.timeSlowLevel++;
                        curLevel++;
                        break;
                    case 3:
                        Player.scoreX2Level++;
                        curLevel++;
                        break;                  
                    case 4:
                        Player.bombLevel++;
                        curLevel++;
                        break;
                }
                RefreshItem();
            }           
        }
    }
}
