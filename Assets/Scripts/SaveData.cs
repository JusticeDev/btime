﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System;

public class SaveData : MonoBehaviour {

    public static SaveData instance;

    [Serializable]
    class ScoreData
    {
        public int topScore = 0;
        public float musicVolume = 0.1f;
        public float sfxVolume = 1;
        public int coins = 0;
        public bool ctrlSwapped = false;
        public int immortalLevel = 0;
        public int scoreX2Level = 0;
        public int timeSlowLevel = 0;
        public int bombLevel = 0;
        public bool countActive = false;
    }

    //Объект класса для сохранений
    ScoreData scoreData;

    //Название файла сохранения
    string fileName = "/save.dat";

    // Use this for initialization
    void Start () {
        //Чтобы не уничтожалась при переходе по сценам
        DontDestroyOnLoad(gameObject);

        //Чтобы быть единственным объектом
        if (instance != null)
        {
            Destroy(gameObject);
            return;
        }

        //Инициализируем наш класс с данными сохранения
        scoreData = new ScoreData();

        instance = this;

        //Загружаем сохранение
        LoadSave();
    }

    void Save()
    {
        scoreData.topScore = ScoreManager.topScore;
        scoreData.musicVolume = Settings.musicVolume;
        scoreData.sfxVolume = Settings.sfxVolume;
        scoreData.ctrlSwapped = Settings.swapped;
        scoreData.scoreX2Level = Player.scoreX2Level;
        scoreData.timeSlowLevel = Player.timeSlowLevel;
        scoreData.bombLevel = Player.bombLevel;
        scoreData.immortalLevel = Player.immortalLevel;
        scoreData.coins = Player.coins;
        scoreData.countActive = Settings.countDisabled;

        BinaryFormatter bf = new BinaryFormatter();

        FileStream file = File.Create(Application.persistentDataPath + fileName);

        bf.Serialize(file, scoreData);
        file.Close();
    }

    void LoadSave()
    {
        if (File.Exists(Application.persistentDataPath + fileName))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(Application.persistentDataPath + fileName, FileMode.Open);
            scoreData = (ScoreData)bf.Deserialize(file);
            file.Close();
        }

        ScoreManager.topScore = scoreData.topScore;
        Settings.musicVolume = scoreData.musicVolume;
        Settings.sfxVolume = scoreData.sfxVolume;
        Settings.swapped = scoreData.ctrlSwapped;
        Player.scoreX2Level = scoreData.scoreX2Level;
        Player.timeSlowLevel = scoreData.timeSlowLevel;
        Player.bombLevel = scoreData.bombLevel;
        Player.immortalLevel = scoreData.immortalLevel;
        Player.coins = scoreData.coins;
        Settings.countDisabled = scoreData.countActive;
    }

    //Не вызывается на ведре, но вызывается на ПК
    void OnDestroy()
    {
        if (scoreData != null)
            Save();
    }

    //Для ведра
    void OnApplicationPause()
    {
        if (scoreData != null)
            Save();
    }
}
