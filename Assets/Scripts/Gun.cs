﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour {

    public GameObject projectile;

    //разрешенное время между выстрелами в секундах
    private float timeBetweenShots = 0.5f;

    //Время совершения последнего выстрела
    private float lastShotTime = 0;

    // Use this for initialization
    void Start () {
        //ебанутый хак, чтобы не было тупого бага
        lastShotTime = -timeBetweenShots;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            Shoot();
    }

    public void Shoot()
    {
        if (!Game.paused)
        {
            if (Game.curTime - lastShotTime > timeBetweenShots && !Player.isDead)
            {
                //Создаём пулю, ставим время выстрела
                Instantiate(projectile, transform.position, Quaternion.identity);
                lastShotTime = Game.curTime;
                //anim.SetTrigger("Shoot");
                //GetComponent<AudioSource>().Play();
            }
        }
    }
}
