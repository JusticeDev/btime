﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ModGUI : MonoBehaviour {

    CanvasRenderer rendScore;
    CanvasRenderer rendImmortal;
    CanvasRenderer rendSlowTime;


    // Use this for initialization
    void Start () {
        rendScore = transform.Find("Score Bonus").GetComponent<CanvasRenderer>();
        rendImmortal = transform.Find("Immortality").GetComponent<CanvasRenderer>();
        rendSlowTime = transform.Find("TimeSlow").GetComponent<CanvasRenderer>();
    }
	
	// Update is called once per frame
	void Update () {
        if (ScoreManager.scoremult > 1)
            rendScore.SetColor(Color.white);
        else
            rendScore.SetColor(Color.gray);

        if(Player.immortal)
            rendImmortal.SetColor(Color.white);
        else
            rendImmortal.SetColor(Color.gray);

        if(Time.timeScale < 1)
            rendSlowTime.SetColor(Color.white);
        else
            rendSlowTime.SetColor(Color.gray);
    }
}
