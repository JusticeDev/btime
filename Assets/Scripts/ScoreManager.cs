﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    public static int score; //Текущий скор
    public static int topScore; //Рекорд
    public static int scoremult; //Множитель очков
    public static float starttime; //Когда начал жить множитель
    private static float lifeTime; //Время жизни множителя
    public static float defaultLifeTime = 7;

    //Референс к компоненту текст
    Text text;

    // Use this for initialization
    void Start () {
        //Референсим Текст
        text = GetComponent<Text>();

        //Ну чисто, чтобы не затупить
        if (score != 0)
            score = 0;

        scoremult = 1;       
    }
    
    public static void SetScoreMultiplier(int mult, float dur)
    {
        lifeTime = dur;

        scoremult = mult;
        //Если ускоряем
        if (mult > 1)
            starttime = Game.curTime;            
    }

    // Update is called once per frame
    void Update () {
        //Выставляем наш скор в поле текста
        text.text = "" + score;

        //Возвращаем ускоритель в исходное значение, если мультиплайер своё отжил
        if (Game.curTime - starttime >= lifeTime)
            scoremult = 1;
	}

    public static void AddPoints(int points)
    {
        //Добавляем очки
        score += points * scoremult;
    }
}
