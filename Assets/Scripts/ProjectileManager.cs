﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileManager : MonoBehaviour {

    Rigidbody2D projectile;            

    public GameObject floatText;
    public GameObject explosion;

    Transform backPos;
    Transform frontPos;
    Transform playerPos;
    Renderer rend;

    public static float velocity = 1;				// Скорость нашего снаряда

    //private List<Rigidbody2D> rockets = new List<Rigidbody2D>();

    //private PlayerControl playerCtrl;		// Reference to the PlayerControl script.
    //private Animator anim;                  // Reference to the Animator component.

    //Флаг того, пролетела ли пуля портал или нет
    public bool isFirst = true;

    //Модификаторы:
    public static float scoreSpeedMultiplier = 0;  //мульт скорости
    static int maxScoreMult = 10000; //верхний порог ускорения относительно очков
    private float scoreDiv = 3000.0f; //Переменная зависимости ускорения от кол-ва бонусов

    // Use this for initialization
    void Start () {      
        // Инициализируем референсы на наши компоненты  
        projectile = GetComponent<Rigidbody2D>();
        rend = GetComponent<Renderer>();
        rend.material.color = Color.red;
        //anim = transform.root.gameObject.GetComponent<Animator>();
        //Ищем откуда пулька вылетает сзади:
        backPos = GameObject.FindGameObjectWithTag("BackPortal").transform;
        frontPos = GameObject.FindGameObjectWithTag("FrontPortal").transform;
        playerPos = GameObject.FindGameObjectWithTag("Player").transform;
        //playerCtrl = transform.root.GetComponent<PlayerControl>();
        if (Game.flipped)
            transform.localScale *= -1;
    }

    // Update is called once per frame
    void Update () {
        if (!Game.paused && !Player.isDead)
        {
            //Здесь наш кап по очкам
            if (ScoreManager.score <= maxScoreMult)
                scoreSpeedMultiplier = ScoreManager.score / scoreDiv;
            //Здесь просто ставим ускорение
            projectile.velocity = new Vector2(0, (velocity + scoreSpeedMultiplier));

            if (Game.flipped)
                projectile.velocity *= -1;
        }
        else
            projectile.velocity = Vector2.zero;
    }

    public void OnExplode()
    {
        //рандомим наклон эффекта взрыва и создаём его
        Quaternion randomRotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));
        Instantiate(explosion, transform.position, randomRotation);
    }


    void OnTriggerEnter2D(Collider2D col)
    {
        //Проверка на то, что эта пуля может поднимать бонус
        if (isFirst)
        {
            if (col.gameObject.CompareTag("Bonus"))
            {
                //Создаём анимку уничтожения бонуса
                BonusManager bMgr = col.gameObject.GetComponent<BonusManager>();
                bMgr.CreateAnim();

                //Проигрываем звук подбора
                AudioSource bonusSound = col.gameObject.GetComponent<AudioSource>();
                bonusSound.Play();

                //Отключаем рендерер
                col.gameObject.GetComponent<Renderer>().enabled = false;
                col.gameObject.GetComponent<Collider2D>().enabled = false;

                //Всегда даём очки и уничтожаем бонус, помоему так круче    
                //Уничтожаем объект через задержку
                Destroy(col.gameObject, col.gameObject.GetComponent<AudioSource>().clip.length);

                //Добавляем очков
                ScoreManager.AddPoints(ScoreRules.pointsForBonus);

                //Создаём всплывающий текст об очках, существует какой-то баг, поэтому приходится юзать костыль
                var temp = Instantiate(floatText);
                temp.GetComponent<FloatingText>().scoreAdded = ScoreRules.pointsForBonus * ScoreManager.scoremult;
                temp.GetComponent<Transform>().position = new Vector2(transform.position.x, transform.position.y + 0.2f);

                //Делаем чё нужно делать с этим бонусом
                switch (bMgr.type)
                {
                    case 1:
                        //Устанавливаем бонус
                        BonusActivate.hasBonus = true;
                        break;
                    case 2:
                        BonusAuto.SetTimeSlowTimer(BonusAuto.defaultSlowDur + Player.timeSlowLevel);
                        break;
                    case 3:
                        if (Player.curlives + 1 <= Player.maxlives)
                            Player.curlives++;
                        break;
                    case 4:
                        ScoreManager.SetScoreMultiplier(2, ScoreManager.defaultLifeTime);
                        break;
                    case 5:
                        BonusAuto.SetImmortalityTimer(BonusAuto.defaultImmDur + Player.immTime);
                        break;
                    case 6:
                        BonusAuto.SetTimeSlowTimer(2);
                        Game.Flip();
                        PortalTargetManager.targetsHit++;
                        PortalTargetManager.SetTargetRandom();
                        break;
                }
            }
        }

        if (col.gameObject.CompareTag("Player"))
        {
            if (!Player.immortal)
            {
                Player.curlives--;
                EventManager.HealthLost();
                Destroy(gameObject);             
                OnExplode();
            }
        }
        else
        if (col.gameObject.CompareTag("ProjectileTag"))
        {
            //Physics2D.IgnoreCollision(col.gameObject.GetComponent<Collider2D>(), GetComponent<Collider2D>());
            if (col.gameObject.GetComponent<ProjectileManager>().isFirst)
            {
                OnExplode();
                Destroy(gameObject);               
            }
            //ScoreManager.AddPoints(10);
        }
        else
        if (col.gameObject.CompareTag("PortalTag"))
        {
            //Меняем цвет пули, переключаем флаг
            if (isFirst)
            {
                ScoreManager.AddPoints(ScoreRules.pointsForPortal);
                rend.material.color = Color.white;
                isFirst = false;
                //Если это подсвеченный портал
                if (col.gameObject.GetComponent<Portal>().number == PortalTargetManager.curSelected)
                {
                    //Переставляем метку портала-цели
                    PortalTargetManager.SetTargetRandom();
                    PortalTargetManager.targetsHit++;
                    WorldController.instance.ChangeBackGroundRandom();
                }
                //Опять же костыль с позицией, я заёбся
                var temp = Instantiate(floatText);
                temp.GetComponent<FloatingText>().scoreAdded = ScoreRules.pointsForPortal * ScoreManager.scoremult;
                temp.GetComponent<Transform>().position = new Vector2(playerPos.position.x, playerPos.position.y + 0.2f);
            }
            //В зависимости от направления, пуля вылетает либо с одной стороны, либо с другой
            if(!Game.flipped)
                transform.position = new Vector2(transform.position.x, backPos.position.y);
            else
                transform.position = new Vector2(transform.position.x, frontPos.position.y);
        }
    }
}

