﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusAuto : MonoBehaviour {

    static float slowStartTime; //с какого момента замедляем 
    static float slowDur; //на сколько замедляем по времени
    public static float defaultSlowDur = 5;

    static float immStartTime;
    public static float immDur;
    public static float defaultImmDur = 5;

    void Start()
    {
        slowStartTime = 0;
        immStartTime = 0;
        slowDur = 0;
        immDur = 0;
    }

    public static void SetTimeSlowTimer(float dur)
    {
        Time.timeScale = 0.5f;
        slowStartTime = Game.curTime;
        slowDur = dur/2;
    }

    public static void SetImmortalityTimer(float dur)
    {
        Player.SetImmortality(true);
        immStartTime = Game.curTime;
        immDur = dur;
    }

	// Update is called once per frame
	void Update () {
        if (!Game.paused)
        {
            if (Game.curTime - slowStartTime > slowDur)
                Time.timeScale = 1f;

            if (Game.curTime - immStartTime > immDur)
                Player.SetImmortality(false);
        }
    }
}
