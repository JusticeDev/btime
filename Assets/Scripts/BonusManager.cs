﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusManager : MonoBehaviour {
    public int type;
    public int spawnsFrom;
    public float offset;

    //Эффект спавна бонуса
    public GameObject spawnAnim;

    public void CreateAnim()
    {
        //Спавним анимацию спана -_-
        Quaternion randomRotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));
        Instantiate(spawnAnim, transform.position, randomRotation);
    }

    void Start()
    {
        CreateAnim();
    }
}
